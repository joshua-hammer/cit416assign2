<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TimmyTom's</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">

  </head>

  <body>

<!-- Navigation -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
  <div class="container">
    <a class="navbar-brand" href="index.php"><img src="Pics/logo.png" style ="height: 90px; width: 205px;"></a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">

        <li class="nav-item">
          <a class="nav-link" href="index.php">Home</a>
        </li>

      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="menu.php" id="navbarDropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Menu
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenu">
            <a class="dropdown-item" href="menu.php">View Entire Menu</a>
            <a class="dropdown-item" href="subbuilder.php">Build-Ya-Sub</a>
            <a class="dropdown-item" href="grilledsubs.php">Grilled Subs</a>
            <a class="dropdown-item" href="coldsubs.php">Cold Subs</a>
            <a class="dropdown-item" href="wraps.php">Wraps</a>
            <a class="dropdown-item" href="salads.php">Salads</a>
          </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="order.php"><i class="fa fa-shopping-bag" aria-hidden="true"></i></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="order.php">Order</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="catering.php">Catering</a>
      </li>

        <li class="nav-item">
          <a class="nav-link" href="contact.php">Contact</a>
        </li>

      </ul>
    </div>
  </div>
</nav>
