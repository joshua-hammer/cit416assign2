
  <? include('header.php'); ?>

    <header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <!-- Slide One - Set the background image for this slide in the line below -->
          <div class="carousel-item active" style="background-image: url('Pics/HomeLogo.jpg')">
            <div class="carousel-caption d-none d-md-block">
            </div>
          </div>
          <!-- Slide Two - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('Pics/shoplogo.jpg')">
            <div class="carousel-caption d-none d-md-block">
              <h3><a href="https://www.google.com/maps" style ="color: #657222; font-weight: bold;"><h2 style ="text-shadow: 2px 0 black, 0 .5px black, .5px 0 black, 0 .5px black;">Find a Location Near You!</h2></a></h3>
            </div>
          </div>
          <!-- Slide Three - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('Pics/build.jpg')">
            <div class="carousel-caption d-none d-md-block">
              <h3>Build Your Yummy Sandwhich Now!</h3>
              <h4><a href="subbuilder.php">Order Now</a></h4>
            </div>
          </div>
          <div class="carousel-item" style="background-image: url('Pics/sand1.jpg')">
            <div class="carousel-caption d-none d-md-block">
              <h2 style ="text-shadow: 2px 0 black, 0 .5px black, .5px 0 black, 0 .5px black;">Check Out The Sandwhich of The Month!</h2>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </header>

    <!-- Page Content -->
    <div class="container">

      <p class="m-0 text-center text-black"><img src="Pics/textlogo.png" style="height:150px; width:575px;"></p>
      <hr>



      <!-- Portfolio Section -->


      <!-- Features Section -->
      <div class="row">
        <div class="col-lg-6">
          <h2>Featured Sub of the Month</h2>
          <p><strong>Chicken Club :</strong>A breaded bonless juicy chicken breast served on your choice of bread with crispy bacon, lettuce, tomato and your choice of cheese.</p>
          <ul>
            <li>Your Choice of Bread</li>
            <li>Breaded Boneless Chicken</li>
            <li>Crispy Bacon</li>
            <li>Lettuce</li>
            <li>Tomato</li>
            <li>Cheese</li>
          </ul>
        </div>
        <div class="col-lg-6">
          <img class="img-fluid rounded" src="Pics/chickenclub.jpg" alt="" style="height:400px; width 450 px;">
        </div>
      </div>
      <!-- /.row -->
        <hr>
      <!-- Call to Action Section -->
      <div class="row mb-4">
        <div class="col-md-4">
          <p style="font-size:18px;"><strong>Feeling Hungry?</strong></p>
        </div>
        <div class="col-md-4">
          <a class="btn btn-lg btn-secondary btn-block" href="#">Build-Ya-Sub!</a>
        </div>
      </div>


      <h2>Portfolio Heading</h2>

      <div class="row">
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="Pics/grilledsub.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Grilled Subs</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="Pics/coldsub.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Cold Subs</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="Pics/buildsub.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Build-Ya-Sub</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos quisquam, error quod sed cumque, odio distinctio velit nostrum temporibus necessitatibus et facere atque iure perspiciatis mollitia recusandae vero vel quam!</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="Pics/wrap.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Wraps</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-sm-6 portfolio-item">
            <p class="m-0 text-center text-black"><img src="Pics/skull.png" style="height:200px; width:150px;"></p>
        </div>

        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="Pics/salad.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Salads</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
          </div>
        </div>

      <!-- /.row -->
      <hr>





    </div>
    <!-- /.container -->

  <!-- Footer -->
  <? include('footer.php'); ?>
