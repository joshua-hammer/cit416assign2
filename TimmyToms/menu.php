<? include('header.php'); ?>

<!-- Page Content -->
<div class="container" style="margin-top: 80px;">

  <!-- Page Heading/Breadcrumbs -->
  <h1 class="mt-4 mb-3">Menu</h1>

  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="index.php" style="color:#600b91;">Home</a>
    </li>
    <li class="breadcrumb-item active">Menu</li>
  </ol>

  <!-- Content Row -->
  <div class="row">
    <!-- Sidebar Column -->
    <div class="col-lg-3 mb-4">
      <div class="list-group">
        <a href="subbuilder.php" class="list-group-item"style="color:#600b91; font-size: 18px; font-weight: bold;">Build-Ya-Sub</a>
        <a href="grilledsubs.php" class="list-group-item" style="color:#600b91; font-size: 18px; font-weight: bold;">Grilled Sandwiches</a>
        <a href="coldsubs.php" class="list-group-item" style="color:#600b91; font-size: 18px; font-weight: bold;">Cold Subs</a>
        <a href="wraps.php" class="list-group-item" style="color:#600b91; font-size: 18px; font-weight: bold;">Wraps</a>
        <a href="salads.php" class="list-group-item" style="color:#600b91; font-size: 18px; font-weight: bold;">Salads</a>
      </div>
    </div>
    <!-- Content Column -->
    <div class="col-lg-9 mb-4">
      <h2>The World is Your Sandwich.</h2>
      <p>Whatever your tastes bud desire, our sandwiches have what they need. If not go ahead and build the sandwich that YOU need.</p>
      <img src="Pics/variety.jpg" style="height: 500px; width:500px;">
    </div>
  </div>
  <!-- /.row -->

</div>
<!-- /.container -->






<? include('footer.php'); ?>
